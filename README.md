# ansible-cumulus-cisco

This repo contains Ansible playbooks for deploy a Spine and Leaf architecture with Cumulus Network OS And Cisco 9K.
Ansible roles are used.

Go to ```.cisco/``` for see how deploy a fabric with Cisco
Go to ```.cumulus/``` for see how deploy a fabric with Cumulus

For deploy Cumulus Network devices in your EVE-NG environnement, you can use my PyEVENG script.
Deployement for Cisco Nexus 9k is coming soon...

```shell
git clone https://gitlab.com/DylanHamel/python-eveng-api.git

chmod 777 eveng-api.py

# Edit the vm/vm_info.yml file.
vim python-eveng-api/vm/vm_info.yml
#:wq

# Add a interface mapped to you local machine with the subnet 10.0.4.x/24
# vmnet3 for example.
# Edit the architecture/2spines_4leafs.yml file
# Modify line 163 -> add your management interface
vim architecture/2spines_4leafs.yml
#:wq

./eveng-api.py --deploy=./architecture/2spines_4leafs.yml
```

Your lab is deploy and is reachable.
You can run your Ansible playbooks on this network.

* spine01 = mgmt = 10.0.4.101 / loopback = 10.0.0.101
* spine02 = mgmt = 10.0.4.102 / loopback = 10.0.0.102
* leaf01 = mgmt = 10.0.4.201 / loopback = 10.0.0.201
* leaf02 = mgmt = 10.0.4.202 / loopback = 10.0.0.202
* leaf03 = mgmt = 10.0.4.203 / loopback = 10.0.0.203
* leaf04 = mgmt = 10.0.4.204 / loopback = 10.0.0.204

```hosts``` file is correct

Prepare your environnement

```shell
# Install python
python --version

# Download files
git@gitlab.com:DylanHamel/ansible-cumulus-cisco.git
# [OR]
https://gitlab.com/DylanHamel/ansible-cumulus-cisco.git

# Create a python venv
python3 -m venv /path/to/repo/ansible-cumulus-cisco

# Install framework and library
/path/to/repo/ansible-cumulus-cisco/bin/pip  install --upgrade pip
/path/to/repo/ansible-cumulus-cisco/bin/pip install -r requirements.txt

# Verify that Ansible is correctly installed
ansible --version
```
