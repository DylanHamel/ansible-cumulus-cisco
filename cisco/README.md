This folder contains Ansible folder tree for Cisco.
This structure has been given during Cisco Live 2019 - Barcelona

Ansible roles are used. You can initialize a new role with Ansible Galaxy

```shell
/path/to/ansible-cumulus-cisco/bin/ansible-galaxy init deploy_spine
# --force
```

IP addresses and some others things have been changed for be used with EVE-NG network.
* 10.0.4.0/24

Configuration on Cisco 9k
```shell
Abort Auto Provisioning and continue with normal setup ?(yes/no)[n] yes

# Set admin password to DylanH123

Would you like to enter the basic configuration dialog (yes/no): no

swtich# dir bootflash:
       4096    Apr 29 04:31:42 2019  .rpmstore/
       4096    Apr 29 04:32:02 2019  .swtam/
     100648    Apr 29 04:37:59 2019  20190429_043338_poap_26143_init.log
  757450240    Oct 29 14:04:30 2016  nxos.7.0.3.I5.1.bin
       4096    Apr 29 04:33:28 2019  scripts/
       4096    Apr 29 04:33:29 2019  virt_strg_pool_bf_vdc_1/
       4096    Apr 29 04:32:17 2019  virtual-instance/
         59    Apr 29 04:32:07 2019  virtual-instance.conf

swtich# conf t
switch(config)# boot nxos bootflash:nxos.7.0.3.I5.1.bin
Performing image verification and compatibility check, please wait....

swtich# hostname Spine02

Spine02(config)# int mgmt 0
Spine02(config-if)# ip add 10.0.4.202 255.255.255.0
Spine02(config-if)# no shut
Spine02(config-if)# exit

Spine02(config)# feature nxapi
Spine02(config)# nxapi http port 80
Spine02(config)# nxapi https port 443

Spine02(config)# crypto key generate rsa
Spine02(config)# ip ssh source-interface mgmt 0 vrf management
Spine02(config)# exit

Spine02# copy run start
[########################################] 100%
Copy complete.
```

After this configuration you can ping your Cisco Nexux

```shell
~ » ping 10.0.4.202
PING 10.0.4.202 (10.0.4.202): 56 data bytes
Request timeout for icmp_seq 0
64 bytes from 10.0.4.202: icmp_seq=1 ttl=255 time=1.895 ms
64 bytes from 10.0.4.202: icmp_seq=2 ttl=255 time=1.242 ms
64 bytes from 10.0.4.202: icmp_seq=3 ttl=255 time=1.439 ms
```

You can login with SSH

```shell
~ » ssh -l admin 10.0.4.202
User Access Verification
Password:
```

## Deploy "D1" - from template

You can deploy your Spine configuration with Ansible.
It uses Ansible Roles ```deploy_spine```

```shell
/path/to/ansible-cumulus-cisco/bin/ansible-playbook deploy_fabric.yml
```

#### Deploy Spine

```bash
/path/to/ansible-cumulus-cisco/bin/ansible-playbook deploy_spine.yml
```

#### Deploy Leaf

```bash
/path/to/ansible-cumulus-cisco/bin/ansible-playbook deploy_leaf.yml
```

---

You can find config file in ```cisco/roles/deploy_leaf/files/spine02.cfg``` 



## Config Multicast Routing

```shell
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_switch.yml --tags "multicast"
```

#### Only Spine

```bash
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_spine.yml --tags "multicast"
```

#### Only Leaf

```shell
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_leaf.yml --tags "multicast"
```



## Config BGP Routing

```shell
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_switch.yml --tags "bgp"
```

#### Only Spine

```bash
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_spine.yml --tags "bgp"
```

#### Only Leaf

```shell
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_leaf.yml --tags "bgp"
```



## Config VXLAN

```shell
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_switch.yml --tags "vxlan"
```

#### Only Spine

```bash
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_spine.yml --tags "vxlan"
```

```shell
/Volumes/Data/gitlab/ansible-cumulus-cisco/cisco(master*) » ./../bin/ansible-playbook config_spine.yml --tags "vxlan"                          dylan.hamel@MacBook-Pro-de-Dylan
[DEPRECATION WARNING]: nxos_ip_interface is kept for backwards compatibility but usage is discouraged. The module documentation details page may explain more about this
rationale.. This feature will be removed in a future release. Deprecation warnings can be disabled by setting deprecation_warnings=False in ansible.cfg.

PLAY [spine] *******************************************************************************************************************************************************************

TASK [Gathering Facts] *********************************************************************************************************************************************************
ok: [spine02]

TASK [config_spine : Enable VXLAN Feature] *************************************************************************************************************************************
ok: [spine02] => (item=nv overlay)
ok: [spine02] => (item=vn-segment-vlan-based)

TASK [config_spine : Enable NV OverlayVXLAN EVPN Fabric and automation using Ansible] ******************************************************************************************
ok: [spine02]

PLAY RECAP *********************************************************************************************************************************************************************
spine02                    : ok=3    changed=0    unreachable=0    failed=0

------------------------------------------------------------
```



#### Only Leaf

```shell
/path/to/ansible-cumulus-cisco/bin/ansible-playbook config_leaf.yml --tags "vxlan"
```

