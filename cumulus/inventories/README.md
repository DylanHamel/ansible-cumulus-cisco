To generate your inventory files you can use my python script

```shell
https://gitlab.com/DylanHamel/invtentory-file-generator.git
# [OR]
git@gitlab.com:DylanHamel/invtentory-file-generator.git
```

And run it with the following command

```shel
./generate_inventories.py -p /inventories/ch/gva/dc1/host_vars -i /inventories/ch/gva/dc1/hosts -g device_type
```